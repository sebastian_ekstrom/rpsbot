from random import randint

class rps:
    
    def __init__(self, win_cond, p1_name = "Player", p2_name = "Computer"):
        self.p1_score = 0
        self.p2_score = 0
        self.p1_name = p1_name
        self.p2_name = p2_name
        self.p1_move = None
        self.p2_move = None
        self.win_cond = win_cond
        self.in_progress = False

    def result(self): 
        # player 1 wins: 1
        # player 2 wins: 2
        # draw: 0
        return (self.p1_move - self.p2_move) % 3
        
    def parse_move(self, input_str):
        results = {"rock" : 0, "r": 0, "paper" : 1, "p" : 1, "scissors" : 2, "s" : 2}
        if input_str.lower() in results.keys():
            return results[input_str.lower()]
        else:
            return -1
        
    def random_move(self):
        return randint(0, 2)
        
    def round_to_str(self):
        moves = ["Rock", "Paper", "Scissors"]
        result = self.p1_name + ": " + moves[self.p1_move] + "\n"
        result += self.p2_name + ": " + moves[self.p2_move] + "\n"            
        result += "Score: " + self.p1_name + " " + str(self.p1_score) + " - " + self.p2_name + " " + str(self.p2_score)
        return result
        
    def result_to_str(self):
        if self.p1_score > self.p2_score:
            result = self.p1_name
        else:
            result = self.p2_name
        result += " wins! " + str(self.p1_score) + " - " + str(self.p2_score)
        return result
                
    def game_loop(self):
        
        while(self.p1_score < self.win_cond and self.p2_score < self.win_cond):
            p2_move = self.random_move();
            p1_move = self.parse_move(input("Enter your move: "))
            if (p1_move == -1):
                print("Invalid move - you lose!")
                return
            result = self.result()
            if result == 1:
                self.p1_score = self.p1_score + 1
            elif result == 2:
                self.p2_score = self.p2_score + 1
            print(self.round_to_str(p1_move, p2_move))
            
        print(self.result_to_str())
        self.in_progress = False
        
        
        
        
        
if __name__ == "__main__":
    rps_game = rps(3)
    rps_game.game_loop()
