import pirc
import interface
import curses
import sys

# Host and port
HOST = "irc.patwic.com"
PORT = 6667

# IRC server password
PASSWORD = "foobar"

# Create IRC server connection object
irc = pirc.IRC(HOST, PORT, PASSWORD)

# User data
NICK = "sebastian"
REALNAME = "Sebastian Ekström"

# Connect to server and register user
irc.connect(NICK, REALNAME)

# Channel
CHAN = "#patwic"

# Join channel
irc.send("JOIN " + CHAN)


def main(stdscreen):
    global NICK

    ui = interface.TextUI(stdscreen)

    while True:
        msg = irc.read()
        if msg:
            tokens = msg.split(maxsplit = 3)
            name = tokens[0][1:tokens[0].find("!")]
            command = tokens[1]
            if command == "PRIVMSG":
                message = tokens[-1][1:] 
                if (tokens[2] == NICK):
                    ui.output(name + " @ " + NICK + ": " + message)
                else:
                    ui.output(name + ": " + message)
            elif command == "NICK":
                newname = tokens[2][1:]
                ui.output(name + " changed nick to " + newname)
            elif command == "QUIT":
                if (tokens.__len__() >= 3):
                    message = " ".join(tokens[2:])[1:]
                else:
                    message = ""
                ui.output(name + " has quit: " + message)
            else:
                #ui.output(tokens[0] + ": " + tokens[-1])
                ui.output(msg)
        usr_msg = ui.input();
        if usr_msg:
            if usr_msg[0] == "/":
                tokens = usr_msg.split()
                command = tokens[0]
                if command == "/msg":
                    irc.send("PRIVMSG " + tokens[1] + " :" + " ".join(tokens[2:]))
                    ui.output(NICK + " @ " + tokens[1] + ": " +  " ".join(tokens[2:]))
                elif command == "/list":
                    irc.send("NAMES :")
                    ui.output(NICK + ": " + usr_msg)
                elif command == "/nick":
                    NICK = tokens[1]
                    irc.send("NICK :" + NICK)
                elif command == "/quit":
                    if tokens.__len__() > 1:
                        irc.send("QUIT :" + " ".join(tokens[1:]))
                    else:
                        irc.send("QUIT :")
                    sys.exit()
                else:
                    irc.send("PRIVMSG " + CHAN + " :" + usr_msg)
                    ui.output(NICK + ": " +  usr_msg)
            else:
                irc.send("PRIVMSG " + CHAN + " :" + usr_msg)
                ui.output(NICK + ": " +  usr_msg)

curses.wrapper(main)



#[Oskise] hej
#:Oskise!~Oskise@ip-141-255-190-71.kna.citycloud.se PRIVMSG #patwic :hej
