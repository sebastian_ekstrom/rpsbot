import pirc
import interface
import curses
import sys
from rps import *

# Host and port
HOST = "irc.patwic.com"
PORT = 6667

# IRC server password
PASSWORD = "foobar"

# Create IRC server connection object
irc = pirc.IRC(HOST, PORT, PASSWORD)

# User data
NICK = "cookiebot"
REALNAME = "ycz"

# Connect to server and register user
irc.connect(NICK, REALNAME)

# Channel
CHAN = "#patwic"

# Join channel
irc.send("JOIN " + CHAN)

rps_game = rps(0)

def send_message(message, recipient = CHAN):
    irc.send("PRIVMSG " + recipient + " :" + message)

def rps_parse(message, name, ui):
    global rps_game
    
    moves = {"rock" : 0, "r": 0, "paper" : 1, "p" : 1, "scissors" : 2, "s" : 2}
    
    tokens = message.split()
    if len(tokens) == 1:
        return
    input_str = tokens[1]
    if input_str == "start":
        if rps_game.in_progress:
            send_message("Game already in progress.")
            return
        else:
            if len(tokens) < 2:
                usr_msg = "Please provide the nick of your opponent with the start command."
                return
            opponent = tokens[2]
            rps_game = rps(1, name, opponent)
            rps_game.in_progress = True
            send_message(name + " has challenged " + opponent + " to a game of rock-paper-scissors!")
            send_message("Please send me your move with /rps [r/p/s]", name)
            send_message("Please send me your move with /rps [r/p/s]", opponent)
            ui.output("Player 1: " + rps_game.p1_name)
            ui.output("Player 2: " + rps_game.p2_name)
    
    elif input_str in moves:
        if name == rps_game.p1_name:
            ui.output("move " + input_str + " recieved from " + name)
            rps_game.p1_move = rps_game.parse_move(input_str)
        elif name == rps_game.p2_name:
            ui.output("move " + input_str + " recieved from " + name)
            rps_game.p2_move = rps_game.parse_move(input_str)
        else:
            ui.output(name + " " + rps_game.p1_name + " " + rps_game.p2_name)
            return
            
        if rps_game.p1_move != None and rps_game.p2_move != None:
            result = rps_game.result()
            if result == 1:
                rps_game.p1_score = rps_game.p1_score + 1
            elif result == 2:
                rps_game.p2_score = rps_game.p2_score + 1
            round_str = rps_game.round_to_str()
            lines = round_str.split("\n")
            for line in lines:
                send_message(line)
            rps_game.p1_move = None
            rps_game.p2_move = None
            if rps_game.p1_score >= rps_game.win_cond or rps_game.p1_score >= rps_game.win_cond:
                result_str = rps_game.result_to_str()
                lines = result_str.split("\n")
                for line in lines:
                    send_message(line)
                rps_game.in_progress = False
            else:                
                send_message("Please send me your move with /rps [r/p/s]", rps_game.p1_name)
                send_message("Please send me your move with /rps [r/p/s]", rps_game.p2_name)
        


def main(stdscreen):
    global NICK
    global rps_game

    ui = interface.TextUI(stdscreen)
    
    while True:
        msg = irc.read()
        if msg:
            tokens = msg.split(maxsplit = 3)
            name = tokens[0][1:tokens[0].find("!")]
            command = tokens[1]
            if command == "PRIVMSG":
                message = tokens[-1][1:] 
                ui.output(name + ": " + message)
                if message[:4] == "/rps":
                    rps_parse(message, name, ui)
            elif command == "NICK":
                newname = tokens[2][1:]
                ui.output(name + " changed nick to " + newname)
            elif command == "QUIT":
                if (tokens.__len__() >= 3):
                    message = " ".join(tokens[2:])[1:]
                else:
                    message = ""
                ui.output(name + " has quit: " + message)
            else:
                #ui.output(tokens[0] + ": " + tokens[-1])
                ui.output(msg)
                
        usr_msg = ui.input();
        if usr_msg == 'q':
            irc.send("QUIT :shut down")
            exit(0)

curses.wrapper(main)



#[Oskise] hej
#:Oskise!~Oskise@ip-141-255-190-71.kna.citycloud.se PRIVMSG #patwic :hej
